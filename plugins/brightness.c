/*
 * Copyright (c) 2013, KevMoriarty
 * Copyright (c) 2013, Patrick Steinhardt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdbool.h>
#include <stdio.h>
#include <syslog.h>

#include "../config.h"

#define BRIGHTNESS "/sys/class/backlight/" BRIGHTNESS_DEV "/brightness"

static FILE *brightnessFd = 0;
static int brightness;

bool
plugin_init()
{
    brightnessFd = fopen(BRIGHTNESS,"r");

    if (brightnessFd != 0) {
      if (fscanf(brightnessFd, "%i", &brightness) == 1) {
        syslog(LOG_INFO, "brightness state has been initialized.");
      } else {
	syslog(LOG_ERR, "error reading brightness value.\n");
      }
    }

    return brightnessFd != 0;
}

void
plugin_exit()
{
    fclose(brightnessFd);
}

const char *
plugin_status()
{
    static char buf[BRIGHTNESS_LEN];

    if (brightnessFd == 0) {
        return buf;
    }

    fflush(brightnessFd);
    rewind(brightnessFd);

    if (fscanf(brightnessFd, "%i", &brightness) != 1) {
        syslog(LOG_ERR, "error reading brightness value.\n");
        brightnessFd = freopen(BRIGHTNESS, "r", brightnessFd);
    } else {
        snprintf(buf, BRIGHTNESS_LEN, BRIGHTNESS_FORMAT, brightness);
    }

    return buf;
}

char
plugin_format()
{
    return 'B';
}
